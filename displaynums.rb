print "\nshowing nums from 1 to 10\n\n"
1.upto(10){|num|
	print num, "\n"
}
print "\nshowing even nums from 1 to 10\n\n"
1.upto(10){|num|
	if num%2 == 0
		print num, "\n"
	end
}
print "\nshowing prime nums from 1 to 10\n\n"
max=1000000
1.upto(max){|i|
	is_prime = true
	sqrt = Math.sqrt(i).round
	2.upto(sqrt){|m|
		if (i%m) == 0
			is_prime = false
		end
	}
	if is_prime
		print i,"\n"
	end
}

