def fibonacci_func(n)
	i = 0
	j = 1
	f = 0
	1.upto(n-2){|a|
		f = i + j
		i = j
		j = f
	}
	puts "1以上の整数を入力してね" if n < 1
	puts "0" if n == 1
	puts "1" if n == 2
	puts f if n >= 3
end
def refibo(n)
	return 0 if n == 1
	return 1 if n == 2
	refibo(n-1)+refibo(n-2)
end
1.upto(10){|b|
	puts refibo(b)
}
